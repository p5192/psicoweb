﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PsicoWebServicios
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PsicoWebServiciosEntities : DbContext
    {
        public PsicoWebServiciosEntities()
            : base("name=PsicoWebServiciosEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CasosExito> CasosExito { get; set; }
        public virtual DbSet<TratamientosYPadecimientos> TratamientosYPadecimientos { get; set; }
        public virtual DbSet<Pasiente> Pasiente { get; set; }
        public virtual DbSet<PrimerRedaccion> PrimerRedaccion { get; set; }
    }
}
