﻿using PsicoWebServicios.Models;
using PsicoWebServicios.NegocioBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PsicoWebServicios.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ResultInfo<vmInformacionInicial> resultInfo = new ResultInfo<vmInformacionInicial>();
            clsInformacionPagina _clsInformacioPagina = new clsInformacionPagina();
            resultInfo = _clsInformacioPagina.GetInformacionInicial();
            if (!resultInfo.IsError)
            {
                return View(resultInfo.Result);
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Somos un equipo de especialistas en Psicologia con una amplia gama de especialidades y con la experiencia y calidad para aportar tratamientosd con un alto indice de resultados exitosos. \n\r" +
                "Cada unio de nuestros especialistas cuenta con sertificaciónes en diversas áreas de especialidades, tanto e cuantoa tratamientos y metodologias para encontrar cual es la que resulta ser la mas útil para cada paciente.";
            ViewBag.Mision = "Aportar a nuestros pasientes una guia y acompañamiento desde el inicio de su tratamiento, hasta lograr su recuperación con el fin de que logre integrarse en la sociedad de manera funcional.";
            ViewBag.Vision = "Mantener a nuestros profecionales actualizados con los coniocimientos más adelantados en el área, con el fin de contar con las herramientas y conocimientosd de vanguardia que permitan ofreser los mejores tratamientos a nuestros pasientes, de manera que se alcansen los objetivos de cada pasiente.";
            ViewBag.Valores = "Tomar a cada uno de nuestros pasientes como un reto personal que nos permita ver a cada pasiente como persona en toda la extención de la palabra, de manera que la atención para cada uno sea personal";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult GuardaNuevo(clsPasiente _clsPasiente)
        {
            ResultInfo<clsPasiente> resultInfo = new ResultInfo<clsPasiente>();
            clsInformacionPagina _clsInformacioPagina = new clsInformacionPagina();
            resultInfo = _clsInformacioPagina.GuardaNuevo(_clsPasiente);

            if (resultInfo.IsError)
            {
                return Json(new { success = false, result = resultInfo.Result, Message = resultInfo.Message });
            }
            else
            {
                return Json(new { success = true, result =  resultInfo.Result, Message = resultInfo.Message });
            }

            
           
        }


        [HttpPost]
        public JsonResult BuscaExpediente(string Expediente)
        {
            ResultInfo<clsPasiente> resultInfo = new ResultInfo<clsPasiente>();
            clsInformacionPagina _clsInformacioPagina = new clsInformacionPagina();
            resultInfo = _clsInformacioPagina.BuscaExpediente(Expediente);

            if (resultInfo.IsError)
            {
                return Json(new { success = false, result = resultInfo.Result, Message = resultInfo.Message });
            }
            else
            {
                return Json(new { success = true, result = resultInfo.Result, Message = resultInfo.Message });
            }



        }

        [HttpPost]
        public JsonResult RestaFechas(string fechaNacimiento)
        {
            ResultInfo<int> resultInfo = new ResultInfo<int>();
            clsFunciones _clsInformacioPagina = new clsFunciones();
            resultInfo = _clsInformacioPagina.RestaFechas(fechaNacimiento);

            if (resultInfo.IsError)
            {
                return Json(new { success = false, result = resultInfo.Result, Message = resultInfo.Message });
            }
            else
            {
                return Json(new { success = true, result = resultInfo.Result, Message = resultInfo.Message });
            }



        }
    }
}