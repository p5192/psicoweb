﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PsicoWebServicios.Models
{
    public class ResultInfo<T>
    {
        public bool IsError { get; set; }
        public T Result { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public string Accion { get; set; }
    }
}