﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PsicoWebServicios.Models
{
    public class vmInformacionInicial
    {
        public List<CasosExito> lstCasosExito { get; set; }

        public List<TratamientosYPadecimientos> lstTratamientos { get; set; }

        public List<TratamientosYPadecimientos> lstPadecimientos { get; set; }
    }
}