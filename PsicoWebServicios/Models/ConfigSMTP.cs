﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PsicoWebServicios.Models
{
    public class ConfigSMTP
    {
        public string host { get; set; }
        public string port { get; set; }
        public string user { get; set; }
        public string password { get; set; }
        public bool enableSsl { get; set; }
        public bool IsHTML { get; set; }

        public string CorreoDirector { get; set; }
    }
}