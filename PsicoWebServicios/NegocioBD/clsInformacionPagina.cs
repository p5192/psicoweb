﻿using PsicoWebServicios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PsicoWebServicios.NegocioBD
{
    public class clsInformacionPagina
    {
        public ResultInfo<vmInformacionInicial> GetInformacionInicial()
        {
            ResultInfo<vmInformacionInicial> resultInfo = new ResultInfo<vmInformacionInicial>();

            vmInformacionInicial _vmInformacionInicial = new vmInformacionInicial();


            List<CasosExito> listaCasosExito = new List<CasosExito>();
            List<TratamientosYPadecimientos> listaTratamientos = new List<TratamientosYPadecimientos>();
            List<TratamientosYPadecimientos> listaPadesimientos = new List<TratamientosYPadecimientos>();
            using (var Context = new PsicoWebServiciosEntities())
            {
                try
                {

                    listaCasosExito = Context.CasosExito.Where(w => w.Estatus == true).ToList();

                    if (listaCasosExito.Count() != 0)
                    {
                        _vmInformacionInicial.lstCasosExito = new List<CasosExito>();
                        _vmInformacionInicial.lstCasosExito.AddRange(listaCasosExito);
                    }

                    listaTratamientos = Context.TratamientosYPadecimientos.Where(w => w.Estatus == true && w.EsTratamiento == true).ToList();

                    if (listaTratamientos.Count() != 0)
                    {
                        _vmInformacionInicial.lstTratamientos = new List<TratamientosYPadecimientos>();
                        _vmInformacionInicial.lstTratamientos.AddRange(listaTratamientos);
                    }

                    listaPadesimientos = Context.TratamientosYPadecimientos.Where(w => w.Estatus == true && w.EsTratamiento == false).ToList();


                    if (listaPadesimientos.Count() != 0)
                    {
                        _vmInformacionInicial.lstPadecimientos = new List<TratamientosYPadecimientos>();
                        _vmInformacionInicial.lstPadecimientos.AddRange(listaPadesimientos);
                    }

                    resultInfo.IsError = false;
                    resultInfo.Message = "Éxito";
                    resultInfo.Result = _vmInformacionInicial;
                    resultInfo.StatusCode = 200;

                }
                catch (Exception ex)
                {
                    resultInfo.IsError = true;
                    resultInfo.Message = "Error" + ex.Message;
                    resultInfo.Result = _vmInformacionInicial;
                    resultInfo.StatusCode = 500;
                }
            }

            return resultInfo;
        }

        public ResultInfo<clsPasiente> GuardaNuevo(clsPasiente _clsPasiente)
        {

            ResultInfo<clsPasiente> resultInfo = new ResultInfo<clsPasiente>();
            using (var Context = new PsicoWebServiciosEntities())
            {
                clsPasiente clsPasientelOC = new clsPasiente();
                Pasiente Existe = new Pasiente();
                try
                {
                    if (_clsPasiente.Expediente != null)
                    {
                        Existe = Context.Pasiente.Where(w => w.Expediente == _clsPasiente.Expediente).FirstOrDefault();
                    }
                    else
                    {
                        Existe = Context.Pasiente.Where(w => w.Telefono == _clsPasiente.Telefono || w.Nombre == _clsPasiente.Nombre).FirstOrDefault();
                    }



                    if (Existe != null)
                    {
                        if (Existe.Expediente != null)
                        {

                            if (_clsPasiente.Expediente != null)
                            {
                                PrimerRedaccion primerRedaccion = new PrimerRedaccion();
                                primerRedaccion.idPasiente = Existe.idPasiente;
                                primerRedaccion.RedaccionOriginal = _clsPasiente.Descripcion;
                                primerRedaccion.Fecha = DateTime.Now;

                                resultInfo.Message = Existe.Nombre + " Se ha enviado tu solicitud de atención a tu especialista, se comunicará con tigo lo antes posible";
                                resultInfo.IsError = false;
                                resultInfo.Result = clsPasientelOC;
                                resultInfo.Accion = "Existente";
                            }
                            else
                            {
                                resultInfo.Message = Existe.Nombre + " ya cuentas con número de expediente: " + Existe.Expediente + ", favor de utilizar tu expediente para mandar tu solicitúd";
                                resultInfo.IsError = false;
                                resultInfo.Result = clsPasientelOC;
                                resultInfo.Accion = "TieneExpediente";
                            }

                            


                        }
                        else
                        {
                            var EnvioAnterior = Context.PrimerRedaccion.Where(w => w.idPasiente == Existe.idPasiente).OrderByDescending(o => o.Fecha).FirstOrDefault();

                            PrimerRedaccion primerRedaccion = new PrimerRedaccion();
                            primerRedaccion.idPasiente = Existe.idPasiente;
                            primerRedaccion.RedaccionOriginal = _clsPasiente.Descripcion;
                            primerRedaccion.Fecha = DateTime.Now;

                            Context.PrimerRedaccion.Add(primerRedaccion);
                            Context.SaveChanges();

                            resultInfo.IsError = false;
                            resultInfo.Message = "Ya mandaste información con tu descripción y tus datos, espera a que un erxperto se comunique contigo";
                            resultInfo.Accion = "Reenvio";
                            resultInfo.Result = clsPasientelOC;
                            _clsPasiente.Descripcion = _clsPasiente.Descripcion + "&" + EnvioAnterior.RedaccionOriginal + "&" + EnvioAnterior.Fecha;
                        }
                    }
                    else
                    {
                        Pasiente pasiente = new Pasiente();

                        pasiente.Nombre = _clsPasiente.Nombre;
                        pasiente.Sexo = _clsPasiente.Sexo;
                        pasiente.Edad = _clsPasiente.Edad;
                        pasiente.Email = _clsPasiente.Email;
                        pasiente.Ciudad = _clsPasiente.Ciudad;
                        pasiente.Telefono = _clsPasiente.Telefono;
                        pasiente.FechaNacimiento = Convert.ToDateTime(_clsPasiente.FechaNacimiento);

                        Context.Pasiente.Add(pasiente);
                        Context.SaveChanges();

                        PrimerRedaccion primerRedaccion = new PrimerRedaccion();
                        primerRedaccion.idPasiente = pasiente.idPasiente;
                        primerRedaccion.RedaccionOriginal = _clsPasiente.Descripcion;
                        primerRedaccion.Fecha = DateTime.Now;


                        Context.PrimerRedaccion.Add(primerRedaccion);
                        Context.SaveChanges();

                        resultInfo.IsError = false;

                        resultInfo.Accion = "Nuevo";
                        resultInfo.Result = _clsPasiente;



                        resultInfo.Message = "Se ingresó tu información correctamente, resivirás un mensaje de confirmación en la cuenta de correo que proporcionaste y en un plaso menor a 24 horas un especialista te contactará mediante el teléfoino que proporcionaste.";

                    }

                    GestorCorreo functions = new GestorCorreo();


                    var correos = functions.SendEmail(_clsPasiente.Email, _clsPasiente.Nombre, _clsPasiente.Edad, _clsPasiente.Sexo, _clsPasiente.Telefono, _clsPasiente.Ciudad, _clsPasiente.Descripcion, resultInfo.Accion, _clsPasiente.Expediente);

                    return resultInfo;

                }
                catch (Exception EX)
                {
                    resultInfo.IsError = true;
                    resultInfo.Message = "Ocurrió un error al intentar ingresar el registro " + EX.Message;

                    resultInfo.Result = _clsPasiente;
                    return resultInfo;
                }
            }


        }

        public ResultInfo<clsPasiente> BuscaExpediente(string expediente)
        {

            ResultInfo<clsPasiente> resultInfo = new ResultInfo<clsPasiente>();
            using (var Context = new PsicoWebServiciosEntities())
            {
                clsPasiente clsPasiente = new clsPasiente();

                try
                {
                    var Existe = Context.Pasiente.Where(w => w.Expediente == expediente).FirstOrDefault();
                    if (Existe != null)
                    {
                        clsPasiente.Expediente = Existe.Expediente;
                        clsPasiente.Nombre = Existe.Nombre;
                        clsPasiente.Sexo = Existe.Sexo;
                        clsPasiente.Edad = Existe.Edad;
                        clsPasiente.Email = Existe.Email;
                        clsPasiente.Ciudad = Existe.Ciudad;
                        clsPasiente.Telefono = Existe.Telefono;

                        string Mes = (Existe.FechaNacimiento.Month - 1).ToString().Length == 1 ? "0" + (Existe.FechaNacimiento.Month - 1).ToString() : Existe.FechaNacimiento.Month.ToString();
                        string Dia = Existe.FechaNacimiento.Day.ToString().Length == 1 ? "0" + Existe.FechaNacimiento.Day.ToString() : Existe.FechaNacimiento.Day.ToString();
                        clsPasiente.FechaNacimiento = Dia + "/" + Mes + "/" + Existe.FechaNacimiento.Year.ToString();


                        resultInfo.Accion = "Tiene expediente";
                        resultInfo.IsError = false;
                        resultInfo.Message = "Expediente encontrado";
                        resultInfo.Result = clsPasiente;
                        resultInfo.StatusCode = 200;
                    }
                    else
                    {
                        resultInfo.Accion = "No tiene expediente";
                        resultInfo.IsError = true;
                        resultInfo.Message = "Expediente no encontrado";
                        resultInfo.Result = null;
                        resultInfo.StatusCode = 200;
                    }
                    return resultInfo;
                }
                catch (Exception EX)
                {
                    resultInfo.IsError = true;
                    resultInfo.Message = "Ocurrió un error al buscar el expediente " + EX.Message;

                    resultInfo.Result = clsPasiente;
                    return resultInfo;
                }
            }
        }
    }
}