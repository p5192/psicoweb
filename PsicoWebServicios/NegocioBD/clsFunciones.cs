﻿using PsicoWebServicios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PsicoWebServicios.NegocioBD
{
    public class clsFunciones
    {
        public ResultInfo<int> RestaFechas(string fechaNacimiento)
        {
            ResultInfo<int> Result = new ResultInfo<int>();

            DateTime _fechaNacimiento = DateTime.Parse(fechaNacimiento);

            if (_fechaNacimiento > DateTime.Today)
            {
                Result.IsError = true;
                Result.Message = "La fecha de nacimiento no puede ser mayor a la fecha actual";
                Result.Result = 0;
                Result.StatusCode = 500;
                return Result;
            }

            TimeSpan timeResult = (DateTime.Today - _fechaNacimiento);
            DateTime totalTime = new DateTime(timeResult.Ticks);
            int edad = totalTime.Year;

            Result.IsError = false;
            Result.Message = "Calculo exitoso";
            Result.Result = edad - 1;
            Result.StatusCode = 200;

            return Result;
        }


    }
}
