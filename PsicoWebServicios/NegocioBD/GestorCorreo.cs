﻿using PsicoWebServicios.Models;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Text;
using System.Configuration;

namespace PsicoWebServicios.NegocioBD
{
    public class GestorCorreo
    {

        private SmtpClient cliente;

        private MailMessage email;

        ConfigSMTP configSMTP = new ConfigSMTP();


        public GestorCorreo()
        {
            configSMTP.host = ConfigurationManager.AppSettings["host"].ToString();
            configSMTP.port = ConfigurationManager.AppSettings["port"].ToString();
            configSMTP.user = ConfigurationManager.AppSettings["user"].ToString();
            configSMTP.password = ConfigurationManager.AppSettings["password"];
            configSMTP.enableSsl = ConfigurationManager.AppSettings["true"] == "false" ? false : true;
            configSMTP.IsHTML = ConfigurationManager.AppSettings["IsHTML"] == "false" ? false : true;

            configSMTP.CorreoDirector = ConfigurationManager.AppSettings["CorreoDirector"].ToString();
        }



        public bool SendEmail(string EmailTo, String NombrePasiente, short Edad, string Sexo, string Telefono, string Ciudad, string Redaccion, string Accion, string Expediente)
        {

            bool exito = true;

            try
            {

                cliente = new SmtpClient(configSMTP.host, Int32.Parse(configSMTP.port))
                {
                    EnableSsl = configSMTP.enableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(configSMTP.user, configSMTP.password)
                };

                if (Accion == "Nuevo")
                {
                    string body = "Saludos cordiales: \r\n" + "\r\n" + "No estas solo, en un plazo no mayor a 24 horas, nos comunicarémos contigo " + NombrePasiente;
                    email = new MailMessage(configSMTP.user, EmailTo, "Respuesta de PsicoTest", body);
                    email.IsBodyHtml = configSMTP.IsHTML;
                    cliente.Send(email);
                }


                if (Accion != "TieneExpediente")
                {
                    char delimita = Convert.ToChar("&");
                    string[] Descripcion;

                    string subject2 = "Solicitud de Servicio";
                    StringBuilder body2 = new StringBuilder();
                    if (Accion == "Nuevo" || Accion == "Reenvio")
                    {
                        body2.AppendLine("Datos solicitante:");
                    }

                    if (Accion == "Existente")
                    {
                        body2.AppendLine("Datos Pasiente:");
                    }


                    body2.AppendLine("");
                    if (Accion == "Existente")
                    {
                        body2.AppendLine("Expediente: " + Expediente);
                    }
                    body2.AppendLine("Nombre: " + NombrePasiente);
                    body2.AppendLine("Edad:   " + Edad);
                    body2.AppendLine("Sexo:   " + Sexo);
                    body2.AppendLine("Email:  " + EmailTo);
                    body2.AppendLine("Teléfono: " + Telefono);
                    body2.AppendLine("Ciudad: " + Ciudad);
                    body2.AppendLine("Redacción de la solicitud de ayuda: ");
                    if (Accion == "Reenvio")
                    {
                        Descripcion = Redaccion.Split(delimita);
                        body2.AppendLine("Redacción actual:");
                        body2.AppendLine(Descripcion[0]);
                        body2.AppendLine("Redacción anterior:");
                        body2.AppendLine(Descripcion[1]);
                        body2.AppendLine("Fecha redacción anterior:");
                        body2.AppendLine(Descripcion[2]);
                    }
                    else
                    {
                        body2.AppendLine("Redacción: ");
                        body2.AppendLine(Redaccion);
                    }



                    string bodyDireccion = body2.ToString();
                    email = new MailMessage(configSMTP.user, configSMTP.CorreoDirector, subject2, bodyDireccion);
                    email.IsBodyHtml = configSMTP.IsHTML;
                    cliente.Send(email);
                }

            }
            catch (Exception Ex)
            {
                exito = false;
                var error = Ex;
            }
            return exito;
        }
    }
}